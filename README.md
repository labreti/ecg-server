# ECG over WebSocket project (server) #

The purpose of this project is to implement the transmission of an ECG across the Internet using open protocols and low cost devices.

We have used the WebSocket protocol to transmit the ECG signal, and Arduino with a ECG-Shield to fetch the ECG signal, a Raspberry Pi to implement the WebSocket client, a dyno in the Heroku public cloud to implement the server. The ECG is visible in real time using a browser, visiting the page produced by the server.

## Content##

This repository contains the code for the server: it is designed to be deployed on a compute resource in the cloud.

* the requirements.txt file, enabling the automatic deploymnet and configuration of the server
* the Procfile file, enabling the automatic run of the server
* the ecg\_server script, the Python/Flask server
* the template/index.html file, the Jinja-annotated page that renders the ECG

### How to install and operate the server ###

The software is prepared for authomatic deployment on a cloud resource, which depends on the provider. We used and Heroku dyno, and in that case the steps are the following:

* create a new dyno (myecg)
* clone this repo on you PC
* login to heroku from your PC commandline
* cd into the directory of the cloned directory, and add the dyno as a remote repository
```
heroku add-remote myecg.herokuapp.com
```
* finally push on heroku
```
git push heroku master
```

Now see the deployment to take place. When the server is ready, you can access the landing page of the service. To view the ECG of a given patient, set the <userkey> that is indicated in the ecg-client command. For instance:
```
http://myecg.herokuapp.com/fred123
```
where fred123 was indicated on client side after the -u option in the ecg\_client command on client side.

IMPORTANT: note that we use the HTTP protocol, not HTTPS.

